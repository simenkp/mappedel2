package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.Exception.AddException;
import edu.ntnu.idatt2001.model.Patient;
import edu.ntnu.idatt2001.model.PatientRegister;
import edu.ntnu.idatt2001.persistance.Read;
import edu.ntnu.idatt2001.persistance.Write;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class that represents Junit tests for both Read and Write class
 */

public class PersistanceTest {

    /**
     * Class that represents Junit test for Read Class
     */
    @Nested
    class ReadTest{

        /**
         * Test for finding out import from an existing file is successfully handled.
         * Not expecting any exceptions to be caught
         */
        @Test
        public void readPositiveWithoutDiagnosis(){
            try{
                Read read = new Read();
                File file = new File("src/main/resources/readData/testFileReadWithoutDiagnose.csv");
                PatientRegister loadedRegister = read.readCsv(file);

                assertEquals(2, loadedRegister.getPatients().size());

            } catch (IOException e) {
                assertNull(e.getMessage());
            }
        }

        /**
         * Method that test if reading a file with diagnosis is possible
         * Not expecting any exceptions to be caught
         */

        @Test
        public void readPositiveWithDiagnosis(){
            try{
                Read read = new Read();
                File file = new File("src/main/resources/readData/testFileWithDiagnosisRead.csv");
                PatientRegister loadedRegister = read.readCsv(file);

                assertEquals(1, loadedRegister.getPatients().size());

            } catch (IOException e) {
                assertNull(e.getMessage());
            }
        }

        /**
         * A method that test if that the size of the register is wrong
         */
        @Test
        public void readNegative(){
            try{
                Read read = new Read();
                File file = new File("src/main/resources/readData/testFileReadWithoutDiagnose.csv");
                PatientRegister loadedRegister = read.readCsv(file);

                assertNotEquals(1, loadedRegister.getPatients().size());

            } catch (IOException e) {
                assertNull(e.getMessage());
            }
        }

        /**
         * A method that tests if importing a non existing file is possible
         * Expect exception to be caught
         */
        @Test
        public void readNotSupportedNonExistingFile(){
            Read read = new Read();
            File file = new File("wrongFileName.csv");
            try{
                PatientRegister loadedRegister = read.readCsv(file);
                assertEquals(0, loadedRegister.getPatients().size());

            } catch (IOException e) {
                assertEquals("The file with name " + file.getName() + " does not exist" , e.getMessage());
            }
        }


        /**
         * A class that represents Junit test for Write class
         */
        @Nested
        class WriteTest{
            /**
             * A method that tests if writing to an empty csv file is successful
             * @throws AddException
             */
            @Test
            public void writePositive() throws FileNotFoundException {
                Write write = new Write();
                PatientRegister register = new PatientRegister();
                File file = new File("src/main/resources/readData/testFileWrite.csv");

                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

                try{
                    register.addNewPatient(new Patient("11111111111", "Ola", "Nordmann", "-", "Dr.Per"));
                    write.writeToCsv(register, file);
                    assertEquals("Ola;Nordmann;Dr.Per;11111111111;-", bufferedReader.readLine());
                } catch (IOException | AddException e){
                    assertNull(e.getMessage());
                }
            }

            /**
             * A method that tests if writing to a non existing file is possible
             */
            @Test
            public void writeNotSupportedWrongFileName(){
                Write write = new Write();
                PatientRegister register = new PatientRegister();
                File file = new File("WrongFileName.txt");

                try{
                    register.addNewPatient(new Patient("11111111111", "Ola", "Nordmann", "-", "Dr.Per"));
                    write.writeToCsv(register, file);

                } catch (IOException | AddException e) {
                    assertEquals("Could not write to file " + file.getName(), e.getMessage());
                }
            }
        }
    }
}
