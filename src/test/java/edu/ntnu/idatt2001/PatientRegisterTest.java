package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.Exception.AddException;
import edu.ntnu.idatt2001.Exception.RemoveException;
import edu.ntnu.idatt2001.model.Patient;
import edu.ntnu.idatt2001.model.PatientRegister;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A class that test the methods in PatientRegister
 */

public class PatientRegisterTest {
    /**
     * A class that tests for addNewPatient method
     */
    @Nested
    class addNewPatient {
        @Test
        public void addNewPatientPositive() {
            try {
                PatientRegister patientRegister = new PatientRegister();
                Patient patient = new Patient("10000000000", "Ola", "Nordmann", "Forkjølet", "Peter Haug");
                patientRegister.addNewPatient(patient);

                assertTrue(patientRegister.getPatients().contains(patient));

            } catch (IllegalArgumentException | AddException exception) {
                assertNull(exception.getMessage());
            }
        }

        /**
         * A method that tests if the register do not contain the patient when not added.
         * Not expecting any error to be caught.
         */
        @Test
        public void addNewPatientNegative() {
            try {
                PatientRegister patientRegister = new PatientRegister();
                Patient patient = new Patient("10000000000", "Ola", "Nordmann", "Forkjølet", "Peter Haug");

                assertFalse(patientRegister.getPatients().contains(patient));

            } catch (IllegalArgumentException e) {
                assertNull(e.getMessage());
            }
        }

        /**
         * A method that tests if adding a patient with social security number less than 11 ciphers is possible
         * Expecting IllegalArgumentException to be caught
         */
        @Test
        public void addNewPatientNotSupportedSocialSecurityNumber(){
            try {
                PatientRegister patientRegister = new PatientRegister();
                Patient patient = new Patient("10", "Ola", "Nordmann", "Forkjølet", "Peter Haug");
                patientRegister.addNewPatient(patient);

                assertFalse(patientRegister.getPatients().contains(patient));

            } catch (IllegalArgumentException | AddException exception) {
                assertEquals("Social security number has to consist of 11 digits", exception.getMessage());
            }
        }

        /**
         * A method that tests if it it possible to add a patient without a first name
         * Expect IllegalArgumentException to be caught
         */
        @Test
        public void addNewPatientNotSupportedFirstName(){
            try {
                PatientRegister patientRegister = new PatientRegister();
                Patient patient = new Patient("10000000000", "", "Nordmann", "Forkjølet", "Peter Haug");
                patientRegister.addNewPatient(patient);

                assertFalse(patientRegister.getPatients().contains(patient));

            } catch (IllegalArgumentException | AddException exception) {
                assertEquals("Firstname or lastname cannot be blank", exception.getMessage());
            }
        }

        /**
         * A method that tests if it is possible to add a patient without a last name
         */
        @Test
        public void addNewPatientNotSupportedLastName(){
            try {
                PatientRegister patientRegister = new PatientRegister();
                Patient patient = new Patient("10000000000", "Ola", "", "Forkjølet", "Peter Haug");
                patientRegister.addNewPatient(patient);

                assertFalse(patientRegister.getPatients().contains(patient));

            } catch (IllegalArgumentException | AddException exception) {
                assertEquals("Firstname or lastname cannot be blank", exception.getMessage());
            }
        }

        /**
         * A method that test if it possible to add an already existing patient to the register
         * Expecting an AddException to be caught
         */
        @Test
        public void addAlreadyExistingPatient(){
            try{
                PatientRegister patientRegister = new PatientRegister();
                Patient patient = new Patient("10000000000", "Ola", "Nordmann", "Forkjølet", "Peter Haug");
                patientRegister.addNewPatient(patient);

                //Adding the same patient to the register
                patientRegister.addNewPatient(patient);

            } catch (IllegalArgumentException | AddException e) {
                assertEquals( "Ola Nordmann already exists", e.getMessage());
            }
        }

        /**
         * A method that tests if it is possible to ass a null patient
         * Expecting NullPointerException to be caught
         */
        @Test
        public void addNullPatient(){
            try {
                PatientRegister patientRegister = new PatientRegister();
                patientRegister.addNewPatient(null);
            }
            catch (AddException | NullPointerException e){
                assertEquals( "A patient cannot be null", e.getMessage());
            }
        }

        /**
         * A class that test deleteSelectedPatient method
         */

        @Nested
        class deleteSelectedPatient{
            /**
             * A method that test for if deletion of an existing patient is successful.
             */

            @Test
            public void deleteSelectedPatientPositive(){
                try {
                    PatientRegister patientRegister = new PatientRegister();
                    Patient patient = new Patient("10000000000", "Ola", "Nordmann", "Forkjølet", "Peter Haug");
                    patientRegister.addNewPatient(patient);

                    assertTrue(patientRegister.getPatients().contains(patient));

                    patientRegister.deleteSelectedPatient(patient);

                    assertFalse(patientRegister.getPatients().contains(patient));

                } catch (IllegalArgumentException | RemoveException | AddException exception) {
                    assertNull(exception.getMessage());
                }
            }

            /**
             * Method that test for if deletion of a non existing patient is possible
             * expecting IllegalArgumentException to be caught
             */

            @Test
            public void deleteSelectedPatientNegative(){
                try {
                    PatientRegister patientRegister = new PatientRegister();
                    Patient patient = new Patient("10000000000", "Ola", "Nordmann", "Forkjølet", "Peter Haug");

                    assertFalse(patientRegister.getPatients().contains(patient));

                    patientRegister.deleteSelectedPatient(patient);

                } catch (IllegalArgumentException | RemoveException exception) {
                    assertEquals("Ola Nordmann is not registered", exception.getMessage());
                }
            }

            /**
             * A method that tests if deletion of a Null person is possible
             * Expecting NullPointerException to be caught
             */
            @Test
            public void deleteSelectedPatientNotSupportedNull(){
                try{
                    PatientRegister patientRegister = new PatientRegister();
                    patientRegister.deleteSelectedPatient(null);

                } catch (NullPointerException | RemoveException exception){
                    assertEquals("Person cannot be null", exception.getMessage());
                }
            }
        }
    }
}





