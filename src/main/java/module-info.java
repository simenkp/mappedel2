module edu.ntnu.idatt2001 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;

    opens edu.ntnu.idatt2001.controller to javafx.fxml, javafx.base, javafx.graphics;
    opens edu.ntnu.idatt2001 to javafx.fxml, java.base, javafx.graphics;
    opens edu.ntnu.idatt2001.model to javafx.fxml, javafx.base, javafx.graphics;

    exports edu.ntnu.idatt2001.controller;

}