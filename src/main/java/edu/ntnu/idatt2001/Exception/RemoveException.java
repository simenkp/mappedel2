package edu.ntnu.idatt2001.Exception;

/**
 * A class that represents a RemoveException
 */

public class RemoveException extends Throwable {
    private static final long serialVersionUID = 1L;
    private String exception;

    public RemoveException(String exception){
        super(exception);
    }

}
