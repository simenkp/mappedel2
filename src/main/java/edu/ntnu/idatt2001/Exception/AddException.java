package edu.ntnu.idatt2001.Exception;

/**
 * A class that represents an addException
 */

public class AddException extends Throwable {
    private static final long serialVersionUID = 1L;
    private String exception;

    public AddException(String exception){
        super(exception);
    }
}
