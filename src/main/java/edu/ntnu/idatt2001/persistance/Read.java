package edu.ntnu.idatt2001.persistance;

import edu.ntnu.idatt2001.Exception.AddException;
import edu.ntnu.idatt2001.model.Patient;
import edu.ntnu.idatt2001.model.PatientRegister;

import java.io.*;
import java.nio.file.Path;

/**
 * A class that read a csv file line for line by usage of a BufferedWriter and place it in a register of type PatientRegister
 * Use try-with resource, that close the file afterwards automatically
 */

public class Read {

    public PatientRegister readCsv(File file) throws IOException{
        PatientRegister loadedRegister = new PatientRegister();

        if(!file.getName().contains(".csv")){
            throw new IOException("The chosen file type is not valid, \nPlease choose a csv file by clicking on OK or cancel the operation");
        }

        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(file))){
            String patientLine;
            //read every line in the file till there is no left and put it in a fixed sized array
            while((patientLine = bufferedReader.readLine()) != null){
                String[] data = patientLine.split(";");
                try{
                    if(data.length == 4){
                        loadedRegister.addNewPatient(new Patient(data[3], data[0], data[1], "-", data[2]));
                    }
                    else if(data.length == 5){
                        loadedRegister.addNewPatient((new Patient(data[3], data[0], data[1], data[4], data[2])));
                    }
                } catch (IllegalArgumentException | AddException e){
                    System.out.println(e.getMessage());
                }
            }
        } catch (IOException e) {
            throw new IOException("The file with name " + file.getName() + " does not exist");
        }
        return loadedRegister;
    }
}
