package edu.ntnu.idatt2001.persistance;

import edu.ntnu.idatt2001.model.Patient;
import edu.ntnu.idatt2001.model.PatientRegister;

import java.io.*;

/**
 * A Class for writing to a csv- file.
 * Looping trough a register and appends every object to the last line of the csv file
 * Use try-with resource, that close the file afterwards automatically
 */

public class Write {
    public void writeToCsv(PatientRegister loadedRegister, File file) throws IOException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))){
            if(!file.getName().contains(".csv")){
                throw new IOException("The chosen file type is not valid, \nPlease choose a .csv-file by clicking on OK or cancel the operation");
            }
            for(Patient p: loadedRegister.getPatients()){
                bufferedWriter.append(p.getFirstName()).append(";")
                        .append(p.getLastName()).append(";")
                        .append(p.getGeneralPractitioner()).append(";")
                        .append(p.getSocialSecurityNumber()).append(";")
                        .append(p.getDiagnosis()).append("\n");
            }
        } catch (IOException e){
            throw new IOException("Could not write to file " + file.getName());
        }

    }
}
