package edu.ntnu.idatt2001.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileStore;
import java.util.List;
import java.util.Optional;


import edu.ntnu.idatt2001.Exception.AddException;
import edu.ntnu.idatt2001.Exception.RemoveException;
import edu.ntnu.idatt2001.model.Patient;
import edu.ntnu.idatt2001.model.PatientRegister;
import edu.ntnu.idatt2001.App;
import edu.ntnu.idatt2001.persistance.Read;
import edu.ntnu.idatt2001.persistance.Write;
import edu.ntnu.idatt2001.view.PatientDialog;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * A class that represents the main controller of the application
 * Handles the main view
 * All the graphical is generated directly in fxml files trough Scenebuilder
 */

public class MainController{

    @FXML
    private Button addNewPatientTool;

    @FXML
    private Button removePatientTool;

    @FXML
    private Button editPatientTool;

    @FXML
    private ToolBar toolBar;

    @FXML
    private MenuItem about;

    @FXML
    private javafx.scene.control.TableView<Patient> patientTable;

    @FXML
    private TableColumn<Patient, String> firstName;

    @FXML
    private TableColumn<Patient, String> lastName;

    @FXML
    private TableColumn<Patient, String> socialSecurityNumber;

    @FXML
    private TableColumn<Patient, String> diagnosis;

    @FXML
    private TableColumn<Patient, String> generalPractitioner;


    /**
     * Method that gets called on load of class
     * Sets up necessary layout and configures it
     * @throws FileNotFoundException
     */
    public void initialize() throws FileNotFoundException {
        columnFactory();
        //Resize the tableview automatically
        patientTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    /**
     * A method that adds a new patient by retrieving the result from PatientDialog
     * and puts it in the main register
     * Updates the tableView afterwards
     */

    @FXML
    public void addPatient() throws AddException {
        PatientDialog contactsDialog = new PatientDialog();
        Optional<Patient> result = contactsDialog.showAndWait();

        if (result.isPresent()) {
            Patient newPatient = result.get();
            PatientRegister register = App.getRegister();
            register.addNewPatient(newPatient);
            App.setRegister(register);
            updateList();
        }
    }

    /**
     * A method that removes a selected patient
     * Updates the tableView afterwards
     * @throws RemoveException
     */
    @FXML
    public void removePatient() throws RemoveException {
        if(patientTable.getSelectionModel().getSelectedItem() == null) {
            showPleaseSelectItemDialog();
        }
        else if(showDeleteConfirmationDialog()){
            PatientRegister register = App.getRegister();
            register.deleteSelectedPatient(patientTable.getSelectionModel().getSelectedItem());
            App.setRegister(register);
            updateList();
        }
    }

    /**
     * A method that edit a selected patient from tableView
     * Edit directly trough PatientDialog class
     * Updates the tableView
     */

    @FXML
    public void editPatient(){
        PatientRegister register = App.getRegister();
        if(patientTable.getSelectionModel().getSelectedItem() == null){
            showPleaseSelectItemDialog();
        }
        else{
            PatientDialog patientDialog = new PatientDialog(patientTable.getSelectionModel().getSelectedItem());
            patientDialog.showAndWait();
            App.setRegister(register);
            updateList();

        }
    }

    /**
     * A method that load an existing csv file to the register
     * not possible to load any other file type than .csv
     * Updates register afterwards
     */

    @FXML
    public void importCsvFile(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a file");
        File file = fileChooser.showOpenDialog(new Stage());
        Read read = new Read();

        try{
            PatientRegister loadedRegister = read.readCsv(file);
            App.setRegister(loadedRegister);
            updateList();
        } catch (IOException e){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Information");
            alert.setHeaderText("Load error");
            alert.setContentText(e.getMessage());

            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                importCsvFile();
            }
        }
    }

    /**
     * A method that write a .csv file with data from register
     * Same content will be overwritten
     * New data will be added last in the file
     * @throws IOException
     */
    @FXML
    public void exportToCsv() throws IOException, NullPointerException {
        PatientRegister loadedRegister = App.getRegister();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        //Prevents the user of saving data in other filetypes than .csv
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(".csv", "*.csv"));
        Write writer = new Write();

        try{
            File file = fileChooser.showSaveDialog(new Stage());

            //Confirmation alert if the created file already exists
            if(file.exists()){
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirmation Dialog");
                alert.setContentText("A file with same name exists. Do you want to overwrite it?" );

                //User can choose to overwrite the file by pressing OK
                Optional<ButtonType> result = alert.showAndWait();
                if(result.get() == ButtonType.OK) {
                    writer.writeToCsv(loadedRegister, file);
                }else{
                    exportToCsv();
                }
            }else{
                writer.writeToCsv(loadedRegister, file);
            }
        } catch (IOException e){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.setTitle("Information");
            alert.setHeaderText("Export error");
            alert.setContentText(e.getMessage());

            Optional<ButtonType> result =  alert.showAndWait();
            if(result.get() == ButtonType.OK){
                exportToCsv();
            }
        }
    }


    /**
     * A private method that set a value for each column in the tableView
     */

    private void columnFactory(){
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumber.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnosis.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitioner.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));

    }

    /**
     * A method that update the tableView by usage of a Observable List.
     */

    public void updateList(){
        patientTable.setItems(App.getPatientRegisterWrapper());
        patientTable.refresh();
    }


    /**
     * A method that show info about the application
     * Information alert dialog
     */

    @FXML
    public void showAbout(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setTitle("About");
        alert.setHeaderText("Patient Register Application");
        alert.setContentText("Version 1.0\n" +
                "Made by Simen Klemp" +
                "\n2021 \u00A9");
            alert.showAndWait();
        }

    /**
     * A method that gives a confirmation alert when pressing exit.
     */

    @FXML
    public void exit(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setContentText("Do you want to exit the application?");

        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK){
            Platform.exit();
        }
    }


    /**
     * A method that gives a warning alert when trying to add, edit or remove an unspecified patient
     */
    @FXML
    public void showPleaseSelectItemDialog() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Information");
        alert.setHeaderText("No items selected");
        alert.setContentText("No item is selected from the table.\n" + "Please select an item from the table.");

        alert.showAndWait();
    }

    /**
     * A method that gives a confirmation alert when trying to delete a patient
     *
     */

    @FXML
    public boolean showDeleteConfirmationDialog() {
        boolean deleteConfirmed = false;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Delete confirmation");
        alert.setHeaderText("Delete confirmation");
        alert.setContentText("Are you sure you want to delete this item?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.isPresent()) {
            deleteConfirmed = (result.get() == ButtonType.OK);
        }
        return deleteConfirmed;
    }
}
