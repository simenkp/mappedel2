package edu.ntnu.idatt2001.view;

import edu.ntnu.idatt2001.model.Patient;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.geometry.Insets;

import java.util.Optional;

/**
 * A class that represents the handling of dialog boxes for adding and editing patients
 * Inherits from Dialog
 */


public class PatientDialog extends Dialog<Patient> {

    public enum Mode {
        NEW, EDIT, INFO
    }

    private final Mode mode;

    private Patient existingPatient = null;

    /**
     * A constructor for adding a new patient
     */
    public PatientDialog() {
        super();
        this.mode = Mode.NEW;
        // Create the content of the dialog
        createPatient();

    }

    /**
     * A constructor for editing a patient
     * @param patient
     */
    public PatientDialog(Patient patient) {
        super();
        this.mode = Mode.EDIT;

        this.existingPatient = patient;
        // Create the content of the dialog
        createPatient();
    }

    /**
     * A method that control the registration or editing of a patient, depending on the mode.
     */
    private void createPatient() {
        // Set title depending upon mode...
        switch (this.mode) {
            case EDIT:
                setTitle("Patient Details - Edit");
                break;

            case NEW:
                setTitle("Patient Details - Add");
                break;

            default:
                setTitle("Patient Details - UNKNOWN MODE...");
                break;

        }

        // Set the button types.
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField firstName = new TextField();
        firstName.setPromptText("First name");

        TextField lastName = new TextField();
        lastName.setPromptText("Last name");

        TextField socialSecurityNumber = new TextField();
        socialSecurityNumber.setPromptText("Social security number");

        // Prohibits the user using letters when typing social security number
        socialSecurityNumber.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    socialSecurityNumber.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        TextField diagnosis = new TextField();
        diagnosis.setPromptText("Diagnosis");

        TextField generalPractitioner = new TextField();
        generalPractitioner.setPromptText("General Practitioner");

        // Fill inn data about the selected patient, if not null.
        if (mode == Mode.EDIT) {

            firstName.setText(existingPatient.getFirstName());
            lastName.setText(existingPatient.getLastName());
            socialSecurityNumber.setText(existingPatient.getSocialSecurityNumber());
            diagnosis.setText(existingPatient.getDiagnosis());
            generalPractitioner.setText(existingPatient.getGeneralPractitioner());
        }

        //Set the title for the respective textfields
        grid.add(new Label("First name:"), 0, 0);
        grid.add(firstName, 1, 0);

        grid.add(new Label("Last name:"), 0,1);
        grid.add(lastName, 1, 1 );

        grid.add(new Label("Social security number:"), 0,2);
        grid.add(socialSecurityNumber, 1, 2);

        grid.add(new Label("Diagnosis:"), 0,3);
        grid.add(diagnosis, 1, 3);

        grid.add(new Label("General practitioner:"), 0,4);
        grid.add(generalPractitioner, 1, 4);

        getDialogPane().setContent(grid);

        setResultConverter((ButtonType button) -> {
            Patient result = null;
            if (button == ButtonType.OK) {

                // Adding a new patient and catching exceptions with try-catch
                if (mode == Mode.NEW) {
                    try{
                        result = new Patient(socialSecurityNumber.getText(), firstName.getText(), lastName.getText(), diagnosis.getText(), generalPractitioner.getText());
                        // warning alert dialog if exceptions are caught when adding new patient
                    }catch (IllegalArgumentException e){
                        Alert alert = new Alert(Alert.AlertType.WARNING);
                        alert.setTitle("Information");
                        alert.setHeaderText("Warning");
                        alert.setContentText(e.getMessage());
                        alert.showAndWait();
                    }
                }
                //Editing a patient directly trough dialog class. Using set-method from Patient.
                //Catching exceptions with try-catch.
            } else if (mode == Mode.EDIT) {
                try{
                    existingPatient.setFirstName(firstName.getText());
                    existingPatient.setLastName(lastName.getText());
                    existingPatient.setDiagnosis(diagnosis.getText());
                    existingPatient.setGeneralPractitioner(generalPractitioner.getText());

                    result = existingPatient;

                } catch (IllegalArgumentException e){
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.setTitle("Information");
                    alert.setHeaderText("Warning");
                    alert.setContentText(e.getMessage());
                    alert.showAndWait();
                }
            }
            //returns the Patient that had been added or edited.
            return result;
            });

    }
}
