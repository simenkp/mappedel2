package edu.ntnu.idatt2001.factory;

/**
 * A class thar represents a Node interface
 * Defines an interface for creating Node objects, but let the subclasses decide which class to instantiate.
 * Factory design pattern provides approach to code for interface rather than implementation
 */
public interface Node {
    public void createNode();
}
