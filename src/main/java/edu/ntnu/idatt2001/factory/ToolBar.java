package edu.ntnu.idatt2001.factory;

/**
 * A class that represents a ToolBar
 * Inherits from Node.
 */

public class ToolBar implements Node {

    /**
     * A method thar overrides the createNode() method in Node.
     * initiates a ToolBar object
     */

    @Override
    public void createNode(){
        Node toolBar = new ToolBar();
    }
}
