package edu.ntnu.idatt2001.factory;

/**
 * A class that represents a MenuBar
 * Inherits from Node.
 */

public class MenuBar implements Node {

    /**
     * A method thar overrides the createNode() method in Node.
     * initiates a MenuBar object
     */


    public void createNode(){
        Node menuBar = new MenuBar();
    }
}
