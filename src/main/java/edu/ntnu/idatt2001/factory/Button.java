package edu.ntnu.idatt2001.factory;

/**
 * A class that represents a Button
 * Inherits from Node
 */

public class Button implements Node {

    /**
     * A method thar overrides the createNode() method in Node.
     * initiates a Button object
     */

    @Override
    public void createNode(){
        Node button = new Button();
    }
}
