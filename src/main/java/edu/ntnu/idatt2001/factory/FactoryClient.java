package edu.ntnu.idatt2001.factory;

/**
 * A class that represents an example client for the FactoryClient
 * Uses the NodeFactory to get objects of Node class by passing an information such as type.
 *
 * Factory pattern removes the instantiation of actual implementation classes from client code.
 * It also makes the code structure more robust, less coupled and easy to extend
 */
public class FactoryClient {
    public static void main(String[] args) {
        Node node1 = NodeFactory.getElement("BUTTON");
        node1.createNode();

        Node node2 = NodeFactory.getElement("MENUBAR");;
        node2.createNode();

        Node node3 = NodeFactory.getElement("TOOLBAR");;
        node3.createNode();
    }
}

