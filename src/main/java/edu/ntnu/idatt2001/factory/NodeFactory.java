package edu.ntnu.idatt2001.factory;

/**
 * A class that represents a factory.
 * Create Node objects without exposing the creation logic to the client
 * Refer to newly created object using a common interface.
 */

public class NodeFactory {

    public static Node getElement(String ElementType){
        if(ElementType == null){
            return null;
        }
        if(ElementType.equalsIgnoreCase("BUTTON")){
            return new Button();
        }
        else if(ElementType.equalsIgnoreCase("MENUBAR")){
            return new MenuBar();
        }
        else if(ElementType.equalsIgnoreCase("TOOLBAR")){
            return new ToolBar();
        }

        return null;
    }
}
