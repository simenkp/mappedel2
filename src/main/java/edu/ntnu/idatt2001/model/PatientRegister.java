package edu.ntnu.idatt2001.model;


import edu.ntnu.idatt2001.Exception.AddException;
import edu.ntnu.idatt2001.Exception.RemoveException;
import edu.ntnu.idatt2001.model.Patient;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * A class that represents a register of patients
 * implements serializable to have the opportunity to store data
 *
 * Does not contain a editPatient method, as I edit a patient directly trough PatientDialog class and have input check in set methods.
 * This only will change the sate of an already existing patient, and will therefore not be necessary to have a edit method here.
 */

public class PatientRegister {
    private ArrayList<Patient> patients;


    public PatientRegister(){
        this.patients = new ArrayList<>();
    }

    public Collection<Patient> getPatients() {
        return this.patients;
    }

    /**
     * A method that add a new patient to the register
     * @param patient the patient that is being added
     * @return returns true if the registration is successful, false if not.
     */

    public boolean addNewPatient(Patient patient) throws AddException, NullPointerException{
        if(patient == null){
            throw new NullPointerException("A patient cannot be null");
        }
        else if(!patients.contains(patient)){
            try {
                patients.add(patient);
                return true;
            }catch (IllegalArgumentException e){
                e.printStackTrace();
                return false;
            }
        }
        else{
            throw new AddException(patient.getFirstName() + " " + patient.getLastName() + " already exists");
        }
    }


    /**
     * A method that delete a specific patient from the register
     *
     * @param patient
     * @return returns true if successfull, false if not
     * @throws RemoveException throws RemoveException when the patient is not registered
     * @throws NullPointerException throws NullPointerException when the trying to delete a Null object
     */

    public boolean deleteSelectedPatient(Patient patient) throws RemoveException, NullPointerException {
        if(patient == null){
            throw new NullPointerException("Person cannot be null");
        }
        else if(!patients.contains(patient)){
            throw new RemoveException(patient.getFirstName() + " " + patient.getLastName() + " is not registered");
        }
        else{
            patients.remove(patient);
            return true;
        }
    }

    /**
     * A method that returns information about all Patient objects.
     * @return returns a string with all patients.
     */

    @Override
    public String toString() {
        String res = "";
        for (Patient patient : patients){
            res += patient.toString() + "\n";
        }
        return res;
    }


}
