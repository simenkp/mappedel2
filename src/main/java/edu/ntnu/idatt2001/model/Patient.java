package edu.ntnu.idatt2001.model;

/**
 * A class that represents a patient
 */

public class Patient {
    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     *
     * @param socialSecurityNumber social security number for a patient
     * @param firstName the patients first name
     * @param lastName the patients last name
     * @param diagnosis the patients diagnosis
     * @param generalPractitioner the patients general practitioner
     * @throws IllegalArgumentException if social security number is less than 11 ciphers or name is blank
     */

    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis, String generalPractitioner) throws IllegalArgumentException{
        if(!(socialSecurityNumber.length() == 11)){
            throw new IllegalArgumentException("Social security number has to consist of 11 digits");
        }
        if(firstName.equals("") || lastName.equals("")){
            throw new IllegalArgumentException("Firstname or lastname cannot be blank");
        }
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.diagnosis = diagnosis;
        this.generalPractitioner = generalPractitioner;

    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }


    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * A method that change the current state of a patients first name
     * @param firstName
     */
    public void setFirstName(String firstName) {
        if(firstName.equals("")|| firstName == null){
            throw new IllegalArgumentException("A patient has to have a first name");
        }
        else{
            this.firstName = firstName;
        }
    }

    /**
     * A method that change the current state of a patients last name
     * @param lastName
     */
    public void setLastName(String lastName) {
        if(lastName.equals("")|| lastName == null){
            throw new IllegalArgumentException("A patient has to have a last name");
        }
        else{
            this.lastName = lastName;
        }
    }

    /**
     * The method checks similarity: two Patient objects are equal if their social security number are identical
     * Identical similarity - two references to the same object
     * Content similarity - the content of two objects is compared by social security number
     * @param o
     * @return returns true if similarity, false if inequality.
     */

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o instanceof Patient){
            Patient tmp = (Patient) o;
            return tmp.getSocialSecurityNumber() == this.socialSecurityNumber;
        }
        return false;
    }

    /**
     * A method that returns information about a Patient object
     * @return returns a String with the information
     */
    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
