package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.controller.MainController;
import edu.ntnu.idatt2001.model.Patient;
import edu.ntnu.idatt2001.model.PatientRegister;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


import java.io.IOException;

/**
 * A class thar represents a JavaFX Application
 *
 * The class is an aggregate of Patient and PatientRegister.
 * Multiplicity between Patient and PatientRegister is "one to many",
 * so both aggregation and composition would fit equally well.
 * Finally, I have chosen aggregation as I see a need to use the Patient objects independently of the PatientRegister, if
 * the registers were to be deleted, I do not want the already registered Patient objects to disappear with it, as this is critical data.
 */
public class App extends Application {

    private Scene scene;
    private static PatientRegister patientRegister = new PatientRegister();
    private static MainController mainController = new MainController();
    private static ObservableList<Patient> patientRegisterWrapper;


    @Override
    public void init() throws Exception {
        super.init();

        // Initialise the main controller
        this.mainController = new MainController();
        this.patientRegister = new PatientRegister();
    }


    /**
     * Runs on the start of the application. Loads the scene "main" and configures it
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("main"), 640, 480);
        stage.setMaximized(true);
        stage.setScene(scene);
        stage.setTitle("PatientApp");
        stage.show();
    }


    /**
     * A static method that updates the observable list.
     * is static because in that way it can be called without inisiating an object
     * @return
     */
    public static ObservableList<Patient> getPatientRegisterWrapper(){
        // Create an ObservableArrayList wrapping the LiteratureRegister
        if (patientRegister == null) {
            patientRegisterWrapper =  null;
        } else {
            patientRegisterWrapper = FXCollections.observableArrayList(patientRegister.getPatients());
        }
        return patientRegisterWrapper;
    }

    /**
     * Class used for loading fxml files
     * @param fxml A string with the path to the fxml file
     * @return the parent
     * @throws IOException If it cant find the fxml file an IOException is thrown
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    /**
     * A method that sets the register
     * @param reg the register we want to set
     */

    public static void setRegister(PatientRegister reg) {
        patientRegister = reg;
        getPatientRegisterWrapper();
    }

    /**
     * get the information about every object in the register
     * @return patientRegister
     */
    public static PatientRegister getRegister(){
        return patientRegister;
    }


    /**
     * The main which will launch the application
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }
}